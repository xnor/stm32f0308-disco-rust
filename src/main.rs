#![feature(proc_macro, lang_items)]
#![no_std]

extern crate cortex_m;
extern crate cortex_m_rtfm as rtfm;
extern crate stm32f030;

use cortex_m::peripheral::SystClkSource;
use rtfm::{app, Threshold};

app! {
    device: stm32f030,

    resources: {
        static ON: bool = false;
    },

    tasks: {
        SYS_TICK: {
            path: toggle,
            resources: [ON, GPIOC, GPIOA],
        },
    },
}

fn init(p: init::Peripherals, _r: init::Resources) {
    p.RCC.ahbenr.write(|w| w.iopcen().set_bit().iopaen().set_bit());
    p.GPIOC.moder.write(|w| unsafe { w.moder9().bits(1).moder8().bits(1) });

    p.GPIOA.moder.write(|w| unsafe { w.moder0().bits(0) }); //input
    p.GPIOA.pupdr.write(|w| unsafe { w.pupdr0().bits(2) }); //pulldown

    p.SYST.set_clock_source(SystClkSource::Core);
    p.SYST.set_reload(500_000);
    p.SYST.enable_interrupt();
    p.SYST.enable_counter();
}

fn idle() -> ! {
    // Sleep
    loop {
        rtfm::wfi();
    }
}

// TASKS

// Toggle the state of the LED
fn toggle(_t: &mut Threshold, r: SYS_TICK::Resources) {
    **r.ON = !**r.ON;

    let on = **r.ON;
    let input = r.GPIOA.idr.read().idr0().bit_is_set();
    r.GPIOC.odr.write(|w| { w.odr9().bit(on).odr8().bit(input) });
}

