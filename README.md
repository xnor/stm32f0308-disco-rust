# blinking lights via rust on stm32f0308-DISCO

Using the [RTFM v2](http://blog.japaric.io/rtfm-v2/) rust crate.

* `xargo build --release`
* update for your paths:
  * `openocd -f /home/alex/local/src/openocd-0.10.0/tcl/board/stm32f0discovery.cfg -f /home/alex/local/src/openocd-0.10.0/tcl/interface/stlink-v2.cfg -c init -c "reset halt"`
* `arm-none-eabi-gdb target/thumbv6m-none-eabi/release/stm32f0308-disco-rust`
